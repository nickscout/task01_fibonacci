
package com.nickscout.main;

import com.nickscout.fibonacci.FibonacciIterativeCalculator;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * Copyright (c) epam systems, is there supposed to be any?
 * I am a true man who stands for opensource!
 */

/**Main file.
 *
 * contains program entry point and describes console UI along with required calculations
 *
 * @author Nick Scout nickscout@protonmail.com
 * @since 30.03.2019
 *
 */

public final class Main {
    /**Main method.
     *
     * User enters interval (for example: [1;100]);
     *
     * Program prints odd numbers from start to the end of interval and even from end to
     * start;
     *
     * Program prints the sum of odd and even numbers;
     *
     * Program build Fibonacci numbers: F1 will be the biggest odd number and F2 – the
     * biggest even number, user can enter the size of set (N);
     *
     * Program prints percentage of odd and even Fibonacci numbers;
     *
     * @param args console launch arguments
     */
    public static void main(String[] args) {
        //start and stop are [a, b] - border of the interval
        int start;
        int stop;

        //reading start, stop via console interface
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Enter start of the interval:\n> ");
        start = inputScanner.nextInt();
        System.out.print("Enter end of the interval:\n> ");

        //since IntStream.range(a,b) excludes b (means .range(1,5) will look like {1,2,3,4}),
        //stop-variable must be incremented
        stop = inputScanner.nextInt() + 1;
        //list of all integers at range from start to stop
        List<Integer> rangeList = IntStream.range(start, stop)
                .boxed()
                .collect(Collectors.toList());
        showRangeList(rangeList);
        //capacity represents amount of fibonacci numbers to count,
        //since next steps require to calculate N forehead fibonacci numbers,
        System.out.print("Enter capacity of the fibonacci numbers storage:\n> ");
        int capacity = inputScanner.nextInt();
        //new instance of iterative calculator class
        FibonacciIterativeCalculator fibonacciCalculator = new FibonacciIterativeCalculator();
        //calculate pseudo-fibonacci sequence with f0 and f1
        // as biggest odd and even number respectively and the size of sequence = capacity variable
        List<Integer> fibonacciNumbers = fibonacciCalculator.getFibonacciNumbersList(
                rangeList.stream()
                        .mapToInt(Integer::intValue)
                        .filter(i -> i % 2 == 0)
                        .max()
                        .getAsInt(),
                rangeList.stream()
                        .mapToInt(Integer::intValue)
                        .filter(i -> i % 2 != 0)
                        .max()
                        .getAsInt(),
                capacity
        );
        //show pseudo-fibonacci sequence
        showFibonacciSquence(fibonacciNumbers);
        //count amount of odd numbers
        //since we know general amount of all numbers, counting even numbers is unnecessary,
        //as we can simply find them by substracting capacity and amount of odd numbers
        int oddNumbersCounter = (int) fibonacciNumbers.stream()
                .filter(integer -> integer % 2 != 0)
                .count();
        //calculate ratio of odd and even numbers in relation to all numbers
        double oddNumbersRate = (double) oddNumbersCounter / capacity;
        double evenNumbersRate = 1 - oddNumbersRate;
        //finally, print percentage of odd and even numbers ratio,
        //don't forget to multiply by 100 so that it looks like a percentage not like a fraction
        System.out.println(String.format("This sequence contains %f percent odd and %f percent even numbers",
                oddNumbersRate * 100,
                evenNumbersRate * 100));
    }

    /** Shows everything related to list of integers in some range.
     *
     * In particular, it prints all odd numbers ascending,
     * then prints all even numbers descending,
     *
     * afterwards prints sum of all odd and even numbers respectively
     *
     * @param rangeList list of various integers
     */
    private static void showRangeList(final List<Integer> rangeList) {
        //show odd numbers from start to end
        StringBuilder oddNumbersAscStringBuilder = new StringBuilder();
        oddNumbersAscStringBuilder.append("Odd numbers ascending: { ");
        rangeList.stream().filter(integer -> integer % 2 == 0).forEachOrdered(integer -> {
            oddNumbersAscStringBuilder.append(integer).append(", ");
        });
        oddNumbersAscStringBuilder.append("};");
        System.out.println(oddNumbersAscStringBuilder.toString());

        //show even numbers from end to start
        StringBuilder evenNumbersDscStringBuider = new StringBuilder();
        evenNumbersDscStringBuider.append("Even numbers descending: { ");
        rangeList.stream().collect(Collectors.collectingAndThen(Collectors.toList(), list -> {
            Collections.reverse(list);
            return list.stream();
        })).filter(integer -> integer % 2 != 0).forEach(integer -> {
            evenNumbersDscStringBuider.append(integer).append(", ");
        });
        evenNumbersDscStringBuider.append("};");
        System.out.println(evenNumbersDscStringBuider.toString());
        //show sum of all odd and even numbers
        System.out.println(String.format("Sum of odd numbers = %d;%nSum of even numbers = %d;%n",
                rangeList.stream().mapToInt(Integer::intValue).filter(i -> i % 2 == 0).sum(),
                rangeList.stream().mapToInt(Integer::intValue).filter(i -> i % 2 != 0).sum()));
    }

    /**Shows list of pseudo fibonacci integers.
     * @param fibonacciNumbers list of pseudo fibonacci integers
     */
    private static void showFibonacciSquence(final List<Integer> fibonacciNumbers) {
        StringBuilder fibonacciNumbersArrayStringBuilder = new StringBuilder();
        fibonacciNumbersArrayStringBuilder.append("Calculated the following fibonacci numbers sequence: { ");
        fibonacciNumbers.stream().forEachOrdered((integer -> {
            fibonacciNumbersArrayStringBuilder.append(integer).append(", ");
        }));
        fibonacciNumbersArrayStringBuilder.append("} ");
        System.out.println(fibonacciNumbersArrayStringBuilder.toString());
    }
}
