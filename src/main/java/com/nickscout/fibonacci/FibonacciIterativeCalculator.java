

package com.nickscout.fibonacci;

/*
 * Copyright (c) epam systems, is there supposed to be any?
 * I am a true man who stands for opensource!
 */

import java.util.ArrayList;

/**Fibonacci calculator inplementation based on iterative numbers addition.
 *
 * This implementation returns ArrayList of N pseudo-fibonacci numbers
 * based on two initial values
 * for example, with f1 = 1, f2 = 2, N = 10
 * the output array will look like {1 2 3 5 8 13 21 34 55 89 144}
 * Although, resulted list is not guaranteed to be pure fibonacci numbers,
 * since f1 nd f2 can be literally anything
 *
 *
 * @author Nick Scout nickscout@protonmail.com
 * @since 30.03.2019
 */

public class FibonacciIterativeCalculator {
    /**The only class method, returns sequence of pseudo Fibonacci numbers.
     * See class documentation for more details
     *
     * @param f1 - first Fibonacci number
     * @param f2 - second Fibonacci number
     * @param capacity - expected size of the list
     * @return ArrayList of pseudo Fibonacci numbers
     */
    public ArrayList<Integer> getFibonacciNumbersList(int f1, int f2, final int capacity) {
        ArrayList<Integer> resultList = new ArrayList<>(capacity);
        resultList.add(f1);
        resultList.add(f2);
        for (int i = 1; i < capacity; i++) {
            int newValue = f1 + f2;
            f1 = f2;
            f2 = newValue;
            resultList.add(f2);
        }
        return resultList;
    }
}
